module.exports = {
    isUserNameValid: function(username) {
        if(username.length<3 || username.length>15) {
            return false;
        }

        // Kob -> kob
        if(username.toLowerCase() !== username) {
            return false;
        }
        return true;
    },

    isAgeValid: function(age) {
        if(typeof age !== 'number') {
            return false;
        }
        if(age<18 || age>100) {
            return false;
        }
        return true;
    }
}